package data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.innerspace.common.models.MacAddress;

public class AristaRssiData {

    private final MacAddress	macAddress;
    private final int			rssi;
    private final int			timeDifferenceSeconds;

    @JsonCreator
    public AristaRssiData(@JsonProperty("MAC") String macAddress, @JsonProperty("rssi") int rssi, @JsonProperty("tdiff") int timeDifferenceSeconds) {

        this.macAddress = MacAddress.tryParse(macAddress);
        this.rssi = rssi;
        this.timeDifferenceSeconds = timeDifferenceSeconds;
    }

    @JsonProperty("MAC")
    public MacAddress getMacAddress() {
        return macAddress;
    }

    @JsonProperty("rssi")
    public int getRssi() {
        return rssi;
    }

    @JsonProperty("tdiff")
    public int getTimeDifferenceSeconds() {
        return timeDifferenceSeconds;
    }

    @Override
    public String toString() {
        return "RssiData{" + "macAddress=" + macAddress + ", rssi=" + rssi + ", timeDifferenceSeconds=" + timeDifferenceSeconds + '}';
    }
}
