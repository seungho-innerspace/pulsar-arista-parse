package data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AristaRawData {
    private final String    sensorMac;
    private final long      timestamp;
    private final AristaData    rawData;

    @JsonCreator
    public AristaRawData(@JsonProperty("s_mac") String sensorMac, @JsonProperty("ts") long timestamp, @JsonProperty("data") AristaData rawData) {
        this.sensorMac = sensorMac;
        this.timestamp = timestamp;
        this.rawData = rawData;
    }

    @JsonProperty("s_mac")
    public String getSensorMac() {
        return sensorMac;
    }

    @JsonProperty("ts")
    public long getTimestamp() {
        return timestamp;
    }

    @JsonProperty("data")
    public AristaData getRawData() {
        return rawData;
    }

    @Override
    public String toString() {
        return "data.AristaRawData [s_mac=" + sensorMac + ", ts=" + timestamp + ", data=" + rawData + "]";
    }
}