package data;

import java.time.Instant;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AristaData {
    private final String				sensorId;
    private final List<AristaRssiData>	rssiData;
    private Instant						timestamp;

    @JsonCreator
    public AristaData(@JsonProperty("lanmac") String sensorId, @JsonProperty("rssidata") List<AristaRssiData> rssiData) {
        this.sensorId = sensorId;
        this.rssiData = rssiData;
    }

    @JsonProperty("lanmac")
    public String getSensorId() {
        return sensorId;
    }

    @JsonProperty("rssidata")
    public List<AristaRssiData> getRssiData() {
        return rssiData;
    }

    @JsonProperty("timestamp")
    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "AristaData{" + "sensorId=" + sensorId + ", rssiData=" + rssiData + ", timestamp=" + timestamp + '}';
    }
}
