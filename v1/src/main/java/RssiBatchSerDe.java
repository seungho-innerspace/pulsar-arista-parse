import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.innerspace.common.models.api.location.RssiBatch;
import org.apache.pulsar.functions.api.SerDe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class RssiBatchSerDe implements SerDe<RssiBatch> {
    private final Logger logger = LoggerFactory.getLogger(RssiBatchSerDe.class);
    private final ObjectMapper objectMapper = new ObjectMapper();

    public RssiBatchSerDe() {
        objectMapper.findAndRegisterModules();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    public RssiBatch deserialize(byte[] input) {
        try {
            String string = new String(input, StandardCharsets.UTF_8);
            return objectMapper.readValue(string, RssiBatch.class);
        } catch (IOException e) {
            logger.error("Failed to deserialize byte[] into RssiBatch from Arista", e);
        }
        return null;
    }

    public byte[] serialize(RssiBatch input) {
        try {
            return objectMapper.writeValueAsString(input).getBytes(StandardCharsets.UTF_8);
        } catch (JsonProcessingException e) {
            logger.error("Failed to serialize RssiBatch for Sensor ID {} from Arista", input.getSensorId(), e);
        }
        return null;
    }
}