import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import data.AristaData;
import data.AristaRawData;
import data.AristaRssiData;
import io.innerspace.common.models.MacAddress;
import io.innerspace.common.models.api.location.ClientId;
import io.innerspace.common.models.api.location.RssiBatch;
import io.innerspace.common.models.api.location.RssiData;
import io.innerspace.common.util.HashUtil;
import org.apache.pulsar.functions.api.Context;
import org.apache.pulsar.functions.api.Function;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class AristaV1RssiParseFunction implements Function<byte[], Void> {
    private static final String UNKNOWN_SENSOR_ID = "unknown-sensor";
    private static final String OUTPUT_NAMESPACE = "innerspace/wifi-v1/";
    private static final String OUTPUT_SERDE_CLASS = "RssiBatchSerDe";
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Void process(byte[] input, Context context) {
        Logger log = context.getLogger();

        objectMapper.findAndRegisterModules();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, false);

        AristaRawData aristaRawData;
        try {
            String string = new String(input, StandardCharsets.UTF_8);
            aristaRawData = objectMapper.readValue(string, AristaRawData.class);
        } catch (IOException e) {
            log.error("Could not parse Arista V1 RSSI data to AristaRawData. Sending to poison queue instead.", e);
            return null;
        }

        if (aristaRawData != null) {
            if (aristaRawData.getSensorMac() == null) {
                log.error("Arista RSSI V1: sensor mac parameter wasn't specified, using {}", UNKNOWN_SENSOR_ID);
                return null;
            }

            AristaData aristaData = aristaRawData.getRawData();
            aristaData.setTimestamp(Instant.ofEpochSecond(aristaRawData.getTimestamp()));
            RssiBatch output = convertToRssiBatch(aristaData);
            String publishTopic = OUTPUT_NAMESPACE + output.getSensorId();
            context.publish(publishTopic, output, OUTPUT_SERDE_CLASS);
        }
        return null;
    }

    private RssiBatch convertToRssiBatch(AristaData aristaData) {
        Instant startTime = aristaData.getTimestamp();
        List<RssiData> rssiData = aristaData.getRssiData().stream().map(aristaRssi -> convertToRssiData(aristaRssi, startTime)).collect(toList());
        Instant endTime = rssiData.stream().map(RssiData::getTimestamp).max(Comparator.naturalOrder()).orElse(startTime);
        return new RssiBatch(startTime, endTime, aristaData.getSensorId(), rssiData);
    }

    private RssiData convertToRssiData(AristaRssiData aristaRssiData, Instant relativeTimestamp) {
        MacAddress oui = aristaRssiData.getMacAddress().getOui();
        byte[] sourceHash = HashUtil.toWifiSHA1(aristaRssiData.getMacAddress().toString());
        ClientId clientId = new ClientId(sourceHash, oui);
        Instant timestamp = relativeTimestamp.plusSeconds(aristaRssiData.getTimeDifferenceSeconds());
        return new RssiData(clientId, timestamp, aristaRssiData.getRssi(), 1, 0);
    }
}
