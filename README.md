# Pulsar Functions for Parsing Arista Data #

Pulsar functions for parsing raw Arista RSSI data into a generic InnerSpace format for use with our dataset generator and location calculators

### Testing ###

By running the function in [localrun mode](http://pulsar.apache.org/docs/en/functions-deploy/#local-run-mode) a pulsar function can be run on a pulsar instance on your local machine while subscribing and publishing data to a remote pulsar broker.



	bin/pulsar-admin functions localrun \
	  --jar /data/cisco-v2-parse-1.0.0.jar \
	  --classname CiscoV2RssiParseFunction \
	  --tenant innerspace \
	  --namespace ingest-wifi-cisco-v2 \
	  --name cisco-v2-parse \
	  --inputs persistent://innerspace/ingest-wifi-cisco-v2/ff05ec12-24d6-45f5-8b0f-1de052d7cf28 \
	  --log-topic persistent://innerspace/function-test/logs \
	  --broker-service-url pulsar://dev.rogsw4rpb5jutkxnc3rffq4dyg.bx.internal.cloudapp.net:6650 \
	  --outputSerdeClassName RssiBatchSerDe



### Deploying ###

To deploy the function to Pulsar a "fat" jar needs to be packaged that contains the function class, its serialization/deserialization ([SerDe](https://pulsar.apache.org/docs/en/functions-develop/#serde)) classes, and dependencies.



 [Deploy Pulsar Functions](http://pulsar.apache.org/docs/en/functions-deploy/)
